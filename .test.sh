#!/bin/bash

clear

DIR=../../ELaSTIC-`cat VERSION`-bin

rm -rf build/*
cd build/
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR
make -j4 install
